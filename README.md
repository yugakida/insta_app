# README

・自分が苦労した点
　　一度ブランチを切ってからdeviseの実装に取り掛かろうとしましたが、
　途中で違うブランチにcheckoutしたら色々トラブってしまいました。


・学んだ点
　　むやみにgitコマンドを叩くのが怖かったので、gitの本を購入して勉強しました。
　　
　　
・自慢したい・相談したい点
　　ログインができるだけで全然インスタの格好をしていませんが、
　二週間たったのでPRさせていただきます。もっと仕上げたほうが
　いいかとは思いますが、PRを待っている間に続きを実装したいと思います。

